=> ../poems.gmi ↩ poems

# Lights
(2020-11-15—2020-12-24)
CC0, contrapunctus/Kashish

As the year draws to a close,
Festive days turn into nights—
I am greeted from the shadows,
By innumerable lights.

The lights, they glow,
Some shine, some twinkle,
Some dazzling, some gentle,
And some of them still—
The tranquil darkness, the bracing breeze,
With cheer and color they fill.

The lights, they glow,
In trains and chains of yellow,
Cascades and parades of red,
Droplets and jewels of blue—
The sights I see all year,
The dull and the drab they make new.

All through the night,
Each lilting light,
Declares to the world around,
"No matter how bleak the world may seem,
No matter the failings of men,
These precious few days,
In view of our gleam,
The beauty you seek will abound."

Oh lights, I fear
That soon we must bid you adieu
It warms my heart to see
In winter's cold, this view
Alas, you go, and I
Will wait another year for you.
