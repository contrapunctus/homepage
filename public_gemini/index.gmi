=> gemini://tilde.team/ ↩ tilde.team

# Hello there!
I'm contrapunctus, a musician who also enjoys writing computer programs.

=> like.gmi things I like
=> gemlog.gmi my gemlog
=> software.gmi my software
=> music-scores.gmi my music scores
=> poems.gmi my poems
=> contact.gmi contact me

Wherever possible, I elevate my work to the public domain, using CC0 for data and Unlicense for code.
=> https://creativecommons.org/share-your-work/public-domain/cc0/ CC0
=> https://unlicense.org/ Unlicense

If you like my work, you can help me pay my rent using Liberapay.
=> https://liberapay.com/contrapunctus
