=> ../gemlog.gmi ↩ gemlog

=> star-wars-nvc.gmi → Next: Star Wars, and non-violent communication
=> web-apps.gmi ← Previous: Reimagining the web as a remote application platform

# Cuis Smalltalk
Installed it yet again today, after watching and being intrigued by a mention of it in this video.
=> https://vimeo.com/71278954 Bret Victor - The Future of Programming

I've run it a few times in the past, and read the Blue Book a bit, but never really wrote any code.

I appreciate simplicity and compactness, so the Cuis values resonate with me.
=> https://github.com/Cuis-Smalltalk/Cuis-Smalltalk-Dev#the-philosophy-behind-cuis The Philosophy Behind Cuis

I like that the window control buttons are all placed to the left corner, rather than some to the right and some to the left. Seems to minimize mouse movement.

I'm /not/ a fan of overlapping windows, but I'm already putting up with them in Cinnamon at the moment, so 🤷‍♀️

There are some spelling errors in the documentation - I might be making a PR.

There'll be more to come in this series, I hope.
