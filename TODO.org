* org-publish
1. Autogenerate titles for web export
2. CSS for files in subdirectories
3. use rsync for transfer
4. Markdown-like syntax in Gemini export - == to ``, quote blocks to >
5. forward/backward arrows in HTML vs in Gemini
6. "blog" on the web, "gemlog" on Gemini (including paths)
7. links in Org vs in Gemini - sometimes I use footnotes, sometimes the link text is different from the inline text...

* TODO [33%]
1. [ ] A download count script for binaries
   https://stackoverflow.com/questions/158124/best-way-to-count-file-downloads-on-a-website
   filename, file size,
2. [X] make HTML page, directing people to the Gemini page
3. [ ] Make a script which automates creation of forward/backward links (gemlog), breadcrumbs, and like/comment links.
