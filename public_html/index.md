# Hello there!
I'm contrapunctus, a musician who also enjoys writing computer programs.

Most of my page is only available over [Gemini](https://gemini.circumlunar.space), a new Internet protocol focusing on simplicity, speed, and privacy. Please install a [Gemini client](https://github.com/kr1sp1n/awesome-gemini#graphical) [1] [2] and visit <gemini://tilde.team/~contrapunctus/>

See you there!

[1] I'd suggest [Lagrange](https://git.skyjake.fi/skyjake/lagrange/releases) (desktop) and DeeDum ([Android](https://f-droid.org/packages/ca.snoe.deedum)/[iOS](https://apps.apple.com/app/deedum/id1546810946)) to the impatient.

[2] Emacs user? Check out [elpher](https://thelambdalab.xyz/elpher/).
