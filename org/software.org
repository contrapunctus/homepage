#+TITLE: Software

[[file:index.org][↩ ~contrapunctus]]

Patches welcome! Check out https://git-send-email.io/ and send me patches at contrapunctus at disroot dot org

All work here is elevated to the public domain using the [[https://unlicense.org/][Unlicense]].

* Common Lisp
:PROPERTIES:
:CUSTOM_ID: common-lisp
:END:
1. [[https://codeberg.org/contrapunctus/clim-app-base][clim-app-base]] - a group of CLIM libraries used in my CLIM applications, including a color theme library

2. [[https://codeberg.org/contrapunctus/dryad][Dryad]] - a [[https://en.wikipedia.org/wiki/Common_Lisp_Interface_Manager][CLIM]] code browser to visualize and edit any program as a tree

3. [[https://codeberg.org/contrapunctus/chronometrist-cl][chronometrist-cl]] - a port of the Chronometrist time tracker to Common Lisp. Includes chronometrist-clim, a CLIM frontend.

4. [[https://codeberg.org/contrapunctus/format-seconds][format-seconds]] - a port of =format-seconds= from Emacs Lisp to Common Lisp

* Emacs Lisp
:PROPERTIES:
:CUSTOM_ID: emacs-lisp
:END:
1. [[https://codeberg.org/contrapunctus/chronometrist][chronometrist]] - a friendly and extensible time tracker and analyzer for Emacs (repository also contains three extensions)

   * [[https://codeberg.org/contrapunctus/chronometrist-goal][chronometrist-goal]] - a time-goal extension for Chronometrist

2. [[https://codeberg.org/contrapunctus/async-backup][async-backup]] - tiny library to backup files on each save without freezing up Emacs

3. [[https://codeberg.org/contrapunctus/sxiv.el][sxiv.el]] - run the Simple X Image Viewer from Emacs, with Dired integration

* Scheme
:PROPERTIES:
:CUSTOM_ID: scheme
:END:
1. [[https://tildegit.org/contrapunctus/mkly][mkly]] - a Guile build script for Lilypond (and other) projects
