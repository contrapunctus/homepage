;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-html-postamble . nil)
              (org-export-with-section-numbers . nil)
              (org-export-with-toc . nil)
              (org-html-self-link-headlines . nil)
              (org-html-head . "<link rel=\"stylesheet\" type=\"text/css\" href=\"../org-doom-molokai.css\" />"))))
